﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.DevTools.V122.Debugger;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OtpNet;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Authentication;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Keys = OpenQA.Selenium.Keys;

namespace TSelenium
{
    public class Browser
    {
        public dynamic remote = null;
        public IWebDriver driver = null;
        public string pathChromeBinary;
        public bool _maximize = false;
        public bool _clean = false;
        public List<IWebElement> elements = new List<IWebElement>();
        
        public Browser(Remote remote, string pathChromeBinary = null)
        {
            this.remote = remote;
            this.pathChromeBinary = pathChromeBinary;
        }
        /// <summary>
        /// Delete the profile directory (if existed), so the browser will be clean, no cached
        /// </summary>
        /// <returns></returns>
        public Browser clean()
        {
            this._clean = true;
            return this;
        }
        public Browser maximize()
        {
            _maximize = true;
            return this;
        }
        public static void UploadFile(IWebDriver driver, By by, string imagePath)
        {
            try
            {
                driver.FindElement(by).SendKeys(imagePath);

            }
            catch (Exception e)
            {

            }
        }
        /// <summary>
        ///    error =  browser_closed,...
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="arrText"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public static void Move(IWebDriver driver, IWebElement el)
        {
            Actions actions = new Actions(driver);
            actions.MoveToElement(el);
            actions.Perform();
        }
        public Tester OpenProfileFake(string pathProfile, string pathForderContains_ChromeDriver, string positionValue = "0,0", string pageOnStartup = "data:,", bool hideBrowse = false)
        {
            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "/chromedriver.exe") == false)
            {
                throw new Exception("chromedriver.exe is not found");
            }
            //CreateBlankProfile - Tạo 1 profile trắng 
            ChromeOptions option = new ChromeOptions();
            if (pathChromeBinary != null)
            {
                string binaryPath = pathChromeBinary;
                binaryPath = binaryPath.Contains(".exe") == false ? binaryPath + ".exe" : binaryPath;
                option.BinaryLocation = binaryPath;
            }


            var driverService = ChromeDriverService.CreateDefaultService(pathForderContains_ChromeDriver);
            driverService.HideCommandPromptWindow = true;
            string[] pageOnStatupS = pageOnStartup.Split(',');
            option.AddArguments("--disable-notifications");
            option.AddArguments("--disable-popup-blocking");
            option.AddArguments("--hide-crash-restore-bubble"); // When the browser is terminated immediately close and open again, Chrome will show a popup, this argument will prevent it : Detail => https://stackoverflow.com/questions/69791724/disable-chrome-restore-pages-chrome-didnt-shut-down-correctly-and-other-brow  
            option.AddArgument("no-sandbox");
            if (hideBrowse)
                option.AddArgument("--headless");
            try
            {
                if (pageOnStatupS[1] == "")
                {
                    option.AddArgument("--homepage \"" + pageOnStatupS[0] + "\"");
                }
                else
                {
                    for (int i = 0; i < pageOnStatupS.Count(); i++)
                    {
                        option.AddArgument("--homepage \"" + pageOnStatupS[i] + "\"");

                    }

                }
            }
            catch
            {
                option.AddArgument("--homepage \"" + pageOnStatupS[0] + "\"");
            }
            #region Set Profile path
            if (Directory.Exists(pathProfile) == false)
                Directory.CreateDirectory(pathProfile);
            option.AddArguments("user-data-dir=" + pathProfile);
            #endregion

            option.AddArguments("--window-size=250,400");
            if(this._clean)
            {
                while(true)
                {
                    try
                    {
                        Directory.Delete(pathProfile, true);
                        break;
                    }catch(Exception e)
                    {
                        if (e.Message.Contains("is denied")) // Có thể browser đang được mở nên không xoá được
                        {
                            TerminateAllBrowser(pathChromeBinary);
                            Thread.Sleep(1000); // Vì nếu Delete liền sẽ bị lỗi gì đó : System.UnauthorizedAccessException: 'Access to the path 'BrowserMetrics-6650A4AD-2148.pma' is denied.'
                        }
                        else
                            throw new Exception(e.Message);
                    }

                }    
            }
            while (true)
            {
                try
                {
                    driver = new ChromeDriver(driverService, option, TimeSpan.FromMinutes(3));
                    break;
                }
                catch (Exception e)
                {
                    if (e.Message.Contains("session not created: Chrome failed to start: exited normally"))
                    {
                        Browser.TerminateAllBrowser(pathChromeBinary);
                    }
                    else
                        throw new Exception(e.Message);
                }
            }
            

            if (positionValue != "0,0") // Nếu positionValue == 0  : thì ta không cần phải set position + size cho trình duyệt
            {
                //Set position
                string[] x8y_string = positionValue.Split(',');
                int X = int.Parse(x8y_string[0]);
                int Y = int.Parse(x8y_string[1]);
                driver.Manage().Window.Position = new System.Drawing.Point(X, Y);



            }
            if (_maximize)
                driver.Manage().Window.Maximize();
            //Check Internet
            CheckErrorInternet();
            return new Tester(this);
        }
        /// <summary>
        /// Require close all browser (except browser create by chromedriver), if not, it is have exception error
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="pathProfile"></param>
        /// <param name="pathForderContains_ChromeDriver"></param>
        /// <param name="positionValue"></param>
        /// <param name="pageOnStartup"></param>
        public void OpenProfileReal(ref IWebDriver driver, string pathProfile, string pathForderContains_ChromeDriver, dynamic remote, string positionValue = "0,0", string pageOnStartup = "data:,")
        {
            //CreateBlankProfile - Tạo 1 profile trắng 
            ChromeOptions option = new ChromeOptions();
            var driverService = ChromeDriverService.CreateDefaultService(pathForderContains_ChromeDriver);
            driverService.HideCommandPromptWindow = true;
            string[] pageOnStatupS = pageOnStartup.Split(',');
            option.AddArguments("--disable-notifications");
            option.AddArguments("--disable-popup-blocking");
            try
            {
                if (pageOnStatupS[1] == "")
                {
                    option.AddArgument("--homepage \"" + pageOnStatupS[0] + "\"");
                }
                else
                {
                    for (int i = 0; i < pageOnStatupS.Count(); i++)
                    {
                        option.AddArgument("--homepage \"" + pageOnStatupS[i] + "\"");

                    }

                }
            }
            catch
            {
                option.AddArgument("--homepage \"" + pageOnStatupS[0] + "\"");
            }
            #region Set Profile path
            string loggedInUser = System.Environment.UserName; //read the Logged in user name
            string[] urlProfileDir_arr = pathProfile.Split('\\');
            string profileDir = urlProfileDir_arr[urlProfileDir_arr.Count() - 1];
            string user_data_dir = "";
            //Bỏ đi phần tử cuối cùng của mảng  urlProfileDir_arr
            for (int i = 0; i < urlProfileDir_arr.Count() - 1; i++)
            {
                user_data_dir += urlProfileDir_arr[i] + "\\";
            }
            //
            option.AddArguments("user-data-dir=" + user_data_dir);
            option.AddArgument("--profile-directory=" + profileDir);
            #endregion

            option.AddArguments("--window-size=250,400");

            driver = new ChromeDriver(driverService, option, TimeSpan.FromMinutes(3));


            if (positionValue != "0,0") // Nếu positionValue == 0  : thì ta không cần phải set position + size cho trình duyệt
            {
                //Set position
                string[] x8y_string = positionValue.Split(',');
                int X = int.Parse(x8y_string[0]);
                int Y = int.Parse(x8y_string[1]);
                driver.Manage().Window.Position = new System.Drawing.Point(X, Y);



            }
            //Check Internet
            CheckErrorInternet();
        }
        public string CheckErrorInternet()
        {
            int i = 0;
        WAITING_INTERNET_OK:
            if (i == 2)
            {
                return "goto_change_ip";
            }
            WaitForReady(TimeSpan.FromSeconds(120));
            Thread.Sleep(3000);
            if (e("#main-frame-error",0).length() > 0)
            {
                driver.Navigate().Refresh();
                i++;
                goto WAITING_INTERNET_OK;

            }
            return "1";
        }
        public static string GetAllCookies(IWebDriver driver)
        {
            string my_cookie = "";
            try
            {
                Actions action = new Actions(driver);
                action.SendKeys(OpenQA.Selenium.Keys.Escape).Perform();
                ReadOnlyCollection<Cookie> cookie = driver.Manage().Cookies.AllCookies;


                List<string> NameCK = new List<string>();

                foreach (var ck in cookie)
                {

                    // Lọc lại các cookie không bị trùng
                    if (NameCK.Count() > 0)
                    {
                        bool isTrung = false;
                        foreach (var n in NameCK)
                        {
                            if (n == ck.Name)
                            {
                                isTrung = true;
                                break;
                            }
                        }
                        if (isTrung) { continue; }
                    }
                    //

                    my_cookie += ck.Name + "=" + ck.Value + ";";


                    NameCK.Add(ck.Name);
                }
                //Remove last character ';'
                my_cookie = my_cookie.Remove(my_cookie.Length - 1, 1);

            }
            catch (Exception e)
            {
                if (e.Message.Contains("no such window") || e.Message.Contains("Object reference not set to an instance of an object")
                      || e.Message.Contains("chrome not reachable"))
                { return "user_close_browser"; }
                else
                {

                }
            }


            return my_cookie;
        }
        public bool go(string url, int timeOut_Seconds = 20)
        {
        START:
            bool isSuccess = false;
            try
            {
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(timeOut_Seconds);  //time to wait untill page is loaded

            }
            catch (Exception e)
            {
                goto START;
            }

            try
            {
                driver.Navigate().GoToUrl(url);  //if page is not loaded in timeOut_Seconds sec exception is thrown
                isSuccess = true;
            }
            catch (Exception e)
            {
                if (e.Message.Contains("timeout"))
                {
                    goto START;
                }
                else
                {
                    goto START;

                }
            }

            return isSuccess;

        }
        public void scrollToBottom(int delay = 2000)
        {
            script("window.scrollTo(0, document.body.scrollHeight);");
            Thread.Sleep(delay);
        }
        public void scrollTo(int top,int delay = 2000)
        {
            script("window.scrollTo({ top: "+ top + ", behavior: 'smooth' });");
            Thread.Sleep(delay);
        }
        /// <summary>
        /// Set or get html content (like Jquery)
        /// </summary>
        /// <param name="content">If the content is empty (default), this method will get HTML content, else it will set HTML content</param>
        /// <returns>The content of html</returns>
        public string html(string content = "")
        {
            if (content == "")
            {
                if (this.elements.Count > 0)
                    return this.elements[0].GetAttribute("innerHTML");
                else
                    return driver.PageSource;
            }    
            else
            {
                script($"arguments[0].innerHTML = `{content}`", elements[0]);
                return content;
            }
        }
        public IWebDriver GetDriver()
        {
            return driver;
        }
        public static void SetTitle(IWebDriver driver, string title)
        {
            ((IJavaScriptExecutor)driver).ExecuteScript(
               "document.title = arguments[0]"
             , title);
        }
        public static bool SeeRecaptra(IWebDriver driver)
        {
            return driver.PageSource.Contains("reCAPTCHA");
        }
        public Browser e(string cssSelector_or_Xpath, int timeWait = 30)
        {
            elements.Clear();
            int s = 0;
            while (s <= timeWait)
            {
                if(cssSelector_or_Xpath.Contains("/"))
                    elements = driver.FindElements(By.XPath(cssSelector_or_Xpath)).ToList();
                else
                    elements = driver.FindElements(By.CssSelector(cssSelector_or_Xpath)).ToList();

                if (elements.Count() == 0)
                {
                    s++;
                    Thread.Sleep(1000);
                    continue;
                }
                break;
               
            }
            return this;
        }
        /// <summary>
        /// Get element matched with textContent or placeholder (input tag)
        /// Mode:
        ///     <para>- 1: Absolute - sẽ tìm kiếm bằng cách gán = (đúng tuyệt đối)</para>
        ///     <para>- 2: Relative - sẽ tìm kiếm bằng cách chỉ trong đoạn text có chứa 1 đoạn text mà tham số truyền vào (đúng tương đối)</para>
        /// </summary>
        public Browser eText(string selector,string[] text, int mode = 2, int timeWait = 30)
        {
            elements.Clear();
            bool isFind = false;
            int s = 0;
            while(isFind == false && s <= timeWait)
            {
                s++;
                foreach (var txt in text)
                {
                    var txtLower = txt.ToLower();
                    try
                    {

                        var tag = driver.FindElements(By.CssSelector(selector)).ToList();

                        switch (mode)
                        {
                            case 1:
                                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(5);
                                foreach (var t in tag)
                                {
                                    var placeholder = t.GetAttribute("placeholder");
                                    if (t.Text.Trim() == txt || placeholder == txt) // placeholder if the selector is <input>
                                    {
                                        elements.Add(t);
                                        isFind = true;
                                    }
                                }

                                break;

                            case 2:
                                foreach (var t in tag)
                                {
                                    var placeholder = t.GetAttribute("placeholder");
                                    if (t.Text.ToLower().Contains(txtLower) || (placeholder != null && placeholder.ToLower().Contains(txtLower)))
                                    {
                                        elements.Add(t);
                                        isFind = true;
                                    }

                                }



                                break;
                        }
                        if (isFind) break;
                    }
                    catch { }

                }
            }
            


            return this;
        }
        
        public Browser val(string value)
        {
        START:
            try
            {
                elements[0].SendKeys(value);
            }
            catch (Exception e)
            {
                if (html().Contains("Không có kết nối internet"))
                {
                    throw new Exception("Không có kết nối internet");
                }
                else if (e.Message.Contains("timeout"))
                    goto START;
                else if (e.Message.Contains("no such elemen"))
                {
                    throw new Exception("Element is not found");
                }
                else { }
            }
            if(elements.Count() == 0)
                throw new Exception("No element to type text");
            return this;
        }
        public int length()
        {
            return elements.Count();
        }
        public void back()
        {
            try
            {
                driver.Navigate().Back();
                Thread.Sleep(1000);
                WaitForReady();
            }
            catch{}
        }
        public void forward()
        {
            try
            {
                driver.Navigate().Forward();
                Thread.Sleep(1000);
                WaitForReady();
            }
            catch { }
        }
        /// <summary>
        /// Click and delay (optional)
        /// </summary>
        /// <param name="delay">Delay time (ms) - default is 2s</param>
        /// <returns></returns>
        public void click(int delay = 2000)
        {
            if(elements.Count() > 0)
            {
                IWebElement el = elements[0];
                Actions actions = new Actions(driver);
                actions.MoveToElement(el);
                actions.Perform();

                el.Click();
                Thread.Sleep(delay);
                
            }
            else
            {
                throw new Exception("Unknown element to click");
            }
        }
        public void clickByJS(int delay = 2000)
        {
            if (elements.Count() > 0)
            {
                IWebElement el = elements[0];
                Actions actions = new Actions(driver);
                actions.MoveToElement(el);
                actions.Perform();

                script("arguments[0].click()", el);
                Thread.Sleep(delay);

            }
            else
            {
                throw new Exception("Unknown element to click");
            }
        }
        public void enter()
        {
            if(elements.Count > 0)
                elements[0].SendKeys(Keys.Enter);
            else throw new Exception("Unknown element to enter");
        }
        
        public void script(string your_script, IWebElement element = null)
        {
            ((IJavaScriptExecutor)driver).ExecuteScript(your_script, element);
        }
        public static void RemoveElement(IWebDriver driver, IWebElement element)
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].remove()", element);
        }
        /// <summary>
        /// @return null if the browser is not found the element
        /// </summary>
        public IWebElement querySelector(string cssSelector)
        {
            try
            {
                return driver.FindElement(By.CssSelector(cssSelector));
            }
            catch
            {
                return null;
            }
        }
        private bool WaitForAjaxReady(IWebDriver driver, TimeSpan waitTime)
        {
            System.Threading.Thread.Sleep(1000);
            WebDriverWait wait = new WebDriverWait(driver, waitTime);
            try
            {
                return wait.Until<bool>((d) =>
                {
                    if (remote.isStop)
                        return true;
                    bool flag;
                    try { flag = driver.FindElements(By.CssSelector(".waiting, .tb-loading")).Count == 0; } catch { flag = false; }
                    return flag;
                });
            }
            catch (Exception e)
            {
                return false;
            }


        }
        public void WaitForDocumentReady(IWebDriver driver, TimeSpan waitTime)
        {
            var wait = new WebDriverWait(driver, waitTime);
            var javascript = driver as IJavaScriptExecutor;
            if (javascript == null)
                throw new ArgumentException("driver", "Driver must support javascript execution");
            try
            {
                wait.Until((d) =>
                {
                    if (remote.isStop)
                        return true;
                    try
                    {

                        string readyState = javascript.ExecuteScript(
                            "if (document.readyState) return document.readyState;").ToString();
                        return readyState.ToLower() == "complete";
                    }
                    catch (InvalidOperationException e)
                    {
                        return e.Message.ToLower().Contains("unable to get browser");
                    }
                    catch (WebDriverException e)
                    {
                        return e.Message.ToLower().Contains("unable to connect");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });

            }
            catch { }

        }
        private bool WaitForPageLoad(IWebDriver driver, TimeSpan waitTime)
        {
            WaitForDocumentReady(driver, waitTime);
            bool ajaxReady = WaitForAjaxReady(driver, waitTime);
            WaitForDocumentReady(driver, waitTime);
            return ajaxReady;
        }
        public bool WaitForReady(TimeSpan? seconds = null)
        {
            /*
             Return:
            -1: User close broser
            0: Error internet
            1: Normal
             
             999: Lỗi lạ
             */
            if (seconds == null) { seconds = TimeSpan.FromSeconds(120); }

            WebDriverWait wait;
            try
            {
                wait = new WebDriverWait(driver, (TimeSpan)seconds);
                wait.Until(d =>
                {
                    if (WaitForPageLoad(driver, (TimeSpan)seconds))
                    {
                        return true; //Normal
                    }
                    else
                    {
                        return false; //Error internet
                    }
                });
            }
            catch (Exception e)
            {
                return false;
            }
            return false; // Lỗi lạ   

        }
        public static void TerminateAllBrowser(string pathChromeBinary)
        {
            /*
             * If the previous browser is opening, we have this error
             We will close all chrome (pathChromeBinary). So we should use Chromium to not
             affect the main chromes. It only closes all chromium.
             Note: This code is only work with 64x
             References: https://stackoverflow.com/a/50118819
             */
            try
            {
                Process[] chrome = Process.GetProcessesByName("chrome");

                foreach (var chromeProcess in chrome)
                {
                    string fullPath = chromeProcess.MainModule.FileName;
                    if (fullPath.Equals(pathChromeBinary))
                    {
                        chromeProcess.Kill();
                    }
                }
                // Delete all chromedrivers are running in Task Manager
                var chromeDriverInTaskManager = Process.GetProcessesByName("chromedriver");
                foreach (var item in chromeDriverInTaskManager)
                {
                    item.Kill();
                }

            }
            catch(Exception e) { 
                if(e.Message.Contains("A 32 bit processes cannot access modules of a 64 bit process"))
                {
                    throw new Exception("Phải chạy ở 64 bit !!");
                }    
            }
        }
        
        
    }
    public class Remote
    {
        public bool isStop;
        public Remote()
        {
            isStop = false;
        }
    }
    public class Tester
    {
        Browser browser;
        private List<string> ___expectApiCall = new List<string>();
        private List<string> ___notExpectApiCall = new List<string>();
        private const string WINDOW_ID = "lazytesting";
        private int width = 200;
        private int height = 0;
        private string current_pID = "";
        private string current_ListID = "";
        private string ___descript = "";
        public Tester(Browser browser)
        {
            this.browser = browser;
            if (browser.driver == null)
                throw new Exception("driver is null, you must initialization when the browser opened");
            var devTools = (OpenQA.Selenium.DevTools.IDevTools)browser.driver;
            OpenQA.Selenium.DevTools.IDevToolsSession session = devTools.GetDevToolsSession();
            var domains = session.GetVersionSpecificDomains<OpenQA.Selenium.DevTools.V124.DevToolsSessionDomains>();
            domains.Network.ResponseReceived += ResponseReceivedHandler;
            System.Threading.Tasks.Task task = domains.Network.Enable(new OpenQA.Selenium.DevTools.V124.Network.EnableCommandSettings());
            //task.RunSynchronously();
            //task.Wait();
        }
        /// <summary>
        /// pixcel = 0 is auto
        /// </summary>
        public Tester window_width(int pixel)
        {
            width = pixel;
            return this;
        }
        /// <summary>
        /// pixcel = 0 is auto
        /// </summary>
        public Tester window_height(int pixel)
        {
            height = pixel;
            return this;
        }
        public void test(string jobName,Func<bool> lambda)
        {
            if(browser.querySelector("#" + WINDOW_ID) == null)
                createWindow();
            current_pID = Guid.NewGuid().ToString().Replace("-", "_");
            log(jobName);
            try
            {
                setLogStatus(lambda());  
            }catch(Exception e)
            {
                setLogStatus(false);
                if (e.Message[0] == '!')
                    throw new Exception(e.Message + " - " + jobName);
                consoleLog(e.Message + " - " + e.StackTrace);
            }
        }
        public void consoleLog(string o)
        {
            o = o.Replace("`", "\"");
            browser.script(
                   $@"
                    console.error(`Error Testing:`, `{o}`);
                    "
               );
        }
        private void setLogStatus(bool result)
        {
            string colorStatus = result ? "green" : "red";
            colorStatus = $"'{colorStatus}'";
            browser.script(
                   $@"
                    document.getElementById('{current_pID}').style.backgroundColor = {colorStatus};
                    "
               );
        }
        private void setDescript(string content)
        {
            content = $"'{content}'";
            browser.script(
                $@"
                const descript = document.getElementById('{current_ListID}_descript');
                descript.innerHTML = {content};
                "
                );
        }
        private void addDetailLog(string content)
        {
            content = $"'{content}'";
            browser.script(
                $@"
                const ul = document.getElementById('{current_ListID}');
                const li = document.createElement('li');
                li.innerHTML = {content};
                li.className = 'lazytesting-li';
                ul.prepend(li);
                "
                );
        }
        private void log(string htmlContent)
        {
            htmlContent = $"'{htmlContent}'";
            browser.script(
                    $@"
                    const div = document.querySelector('#{WINDOW_ID}');
                    const p = document.createElement('p');
                    p.id = '{current_pID}';
                    p.innerHTML = {htmlContent};
                    p.style = 'margin-top: 5px!important;';

                    
                    div.prepend(p);
                    "
                );
        }
        private void createWindow()
        {
            string myHeight = "", myWidth = "";

            if (height == 0)
                myHeight = "auto";
            if (width == 0)
                myWidth = "auto";
            if (myWidth != "auto")
                myWidth += "px";
            if (myHeight != "auto")
                myHeight += "px";
            string idTime = $"{WINDOW_ID}_time";
            const string lazycodet_testing_done = "window.lazycodet_testing_done";
            string setInterval = "setInterval(()=>{" +
                "const time = document.getElementById('"+ idTime + "');" +
                "time.innerHTML =  parseInt(time.innerHTML) + 1;" +
                "},1000);";
            browser.script(
                    $@"
                    const div = document.createElement('div');
                    div.style.width = '{myWidth}';
                    div.style.height = '{myHeight}';
                    div.style.backgroundColor = 'red';  
                    div.style.position = 'fixed';    
                    div.style.top = '0';    
                    div.style.right = '0';  
                    div.style.zIndex = '99999';
                    div.style.backgroundColor = '#7f7979';
                    div.style.color = '#fff';
                    div.style.padding = '10px';
                    div.style.cursor = 'pointer';
                    div.className = 'noselect';

                    const time = document.createElement('p');
                    time.id = '{idTime}';
                    time.innerHTML = '0';

                    const body = document.createElement('div');
                    body.id = '{WINDOW_ID}';

                    div.appendChild(time);
                    div.appendChild(body);

                    document.querySelector('body').appendChild(div);
                    {lazycodet_testing_done} = false;
                    {setInterval}    
                    {event_move_div_by_clicking_and_dragging()}
                    "
                );
            browser.script(
                @"
                    var style = document.createElement('style');
                    style.type = 'text/css';
                    var head = document.head || document.getElementsByTagName('head')[0];
                    head.appendChild(style);" +
                    @"
                    if (style.styleSheet){
                      // This is required for IE8 and below.
                      style.styleSheet.cssText = " + css() + @";
                    } else {
                      style.appendChild(document.createTextNode(" + css() + @"));
                    }
                "
                );
        }
        private string event_move_div_by_clicking_and_dragging()
        {
            // References: https://stackoverflow.com/a/24050777
            return @"
            var mousePosition;
            var offset = [0,0];
            var isDown = false;
            div.addEventListener('mousedown', function(e) {
                isDown = true;
                offset = [
                    div.offsetLeft - e.clientX,
                    div.offsetTop - e.clientY
                ];
            }, true);

            document.addEventListener('mouseup', function() {
                isDown = false;
            }, true);

            document.addEventListener('mousemove', function(event) {
                event.preventDefault();
                if (isDown) {
                    mousePosition = {

                        x : event.clientX,
                        y : event.clientY

                    };
                    div.style.left = (mousePosition.x + offset[0]) + 'px';
                    div.style.top  = (mousePosition.y + offset[1]) + 'px';
                }
            }, true);
            ";
        }
        private string css()
        {
            //References: https://stackoverflow.com/a/4407335
            return @"`
                    .noselect {
                      -webkit-touch-callout: none; /* iOS Safari */
                        -webkit-user-select: none; /* Safari */
                         -khtml-user-select: none; /* Konqueror HTML */
                           -moz-user-select: none; /* Old versions of Firefox */
                            -ms-user-select: none; /* Internet Explorer/Edge */
                                user-select: none; /* Non-prefixed version, currently
                                                      supported by Chrome, Edge, Opera and Firefox */
                    }
                   .lazytesting-li {
                    list-style: disc outside none;
                    display: list-item;
                    margin-left: 1em;
                   }
            `";
        }
        public Tester descript(string text) {
            ___descript = text;
            return this;
        }
        /// <summary>
        /// API calls contain urls
        /// </summary>
        /// <param name="urls"></param>
        /// <returns></returns>
        public Tester expectApiCall(params string[] urls)
        {
            ___expectApiCall = new List<string>(urls);
            return this;
        }
        /// <summary>
        /// API does not call contains urls
        /// </summary>
        /// <param name="urls"></param>
        /// <returns></returns>
        public Tester notExpectApiCall(params string[] urls)
        {
            ___notExpectApiCall = new List<string>(urls);
            return this;
        }

        public void afterDo(Action lambda)
        {
            current_ListID = Guid.NewGuid().ToString().Replace("-", "_");
            browser.script(
                $@"
                    const detailList = document.createElement('ul');
                    detailList.id = '{current_ListID}';
                    
                    const descript = document.createElement('p');
                    descript.id = '{current_ListID}_descript';
                    descript.style.paddingLeft = '5px!important';
                    descript.style.backgroundColor = '#553e13';
                    
                    const current_p = document.getElementById('{current_pID}');
                    insertAfter(current_p, detailList);
                    insertAfter(current_p, descript);
                " +
                @"function insertAfter(referenceNode, newNode) {
                        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
                }"
                );
            lambda();
            if (___descript != "")
                setDescript(___descript);
            int i = 0;
            while(i < 30 && ___expectApiCall.Count() > 0)
            {
                i++;
                Thread.Sleep(1000);
            }
            if (___expectApiCall.Count() > 0)
                throw new Exception("Expect a list of APIs that will be called, but it still has some APIs that are not called");
            i = 0;
            if(___notExpectApiCall.Count() > 0)
            {
                while (i < 5 && ___notExpectApiCall.Count() > 0)
                {
                    i++;
                    Thread.Sleep(1000);
                }
                if (___notExpectApiCall.Count() == 0)
                    throw new Exception("Not Expect a list of APIs that will be called, but it still has some APIs that are called");
            }
            


            ___descript = "";
        }
        bool flag_handlingResponseReceived = false;
        private void ResponseReceivedHandler(object sender, OpenQA.Selenium.DevTools.V124.Network.ResponseReceivedEventArgs e)
        {
            while(flag_handlingResponseReceived == true)
            {
                Thread.Sleep(500);
            }
            flag_handlingResponseReceived = true;
            Console.WriteLine($"Status: { e.Response.Status } : {e.Response.StatusText} | File: { e.Response.MimeType } | Url: { e.Response.Url }");
            for (int i = ___expectApiCall.Count - 1; i >= 0; i--)
            {
                string apiUrl = ___expectApiCall[i];
                // If e.Response.Url contains apiUrl, remove it from ___expectApiCall
                if (e.Response.Url.Contains(apiUrl))
                {
                    addDetailLog("API Called: " + apiUrl);
                    ___expectApiCall.RemoveAt(i);
                    break;
                }
            }
            flag_handlingResponseReceived = false;

        }


    }
}
